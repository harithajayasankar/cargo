package Cargo;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class CargoShipTest {

	@Test
	void test() {
		TimeCalculation time = new TimeCalculation();
		assertEquals(LocalDate.of(2020, 9, 30), time.calculations(LocalDate.of(2020, 9, 22)));
	}

}
