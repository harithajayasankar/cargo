package Cargo;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Scanner;
public class CargoShip {
	
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the starting date");
     	int y = 2020;
		int d = scan.nextInt();
		int m = scan.nextInt();
		LocalDate ld = LocalDate.of( y , m , d  );
		System.out.println(ld);
		TimeCalculation tc = new TimeCalculation();
		LocalDate ld1 = tc.calculations(ld);
		
		
	}
}
class TimeCalculation {
	
	 enum cargShip{
		 DURATION(70),
		 STARTTIME(LocalTime.of(6,0)),
		 ENDTIME(LocalTime.of(18,0));
		
		
		 private int d;
		 private LocalTime t;
		 private cargShip(int i) {
			 this.d = i;
		 }
		 private cargShip(LocalTime t) {
			 this.t = t;
		 }
		 public int get() {
			 return d;
		 }	
		 public LocalTime getTime() {
			 return t;
		 }
		
	}
	
	
	 enum GovtHoliday{
		 JANUARY_1(LocalDate.of(2000, 1, 1)),
		 JANUARY_26(LocalDate.of(2000, 1, 26)),
		 AUGUST_15(LocalDate.of(2000, 8, 15));
		 private LocalDate date;
		 private GovtHoliday(LocalDate date) {
			 this.date = date;
		 }
		 public LocalDate get() {
			 return date;
		 }
	 }
	
	
	
	enum Weekend{
		SATURDAY("Saturday"),
		SUNDAY("Sunday");
		private String day;
		private Weekend(String day) {
			this.day = day;
		}
		public String get() {
			return day;
		}
	}
	
	public LocalDate calculations(LocalDate ld) {
		LocalTime lt = LocalTime.now(ZoneOffset.UTC);
		int hour = lt.getHour();
		int minute = lt.getMinute();
		lt =  LocalTime.of(hour, minute);
		
			
		
		Month mo = ld.getMonth();
		System.out.println(mo);
		int date = ld.getDayOfMonth();
		System.out.println(date);
		int year = ld.getYear();
		System.out.println(year);
		
		//duration
		cargShip dur = cargShip.DURATION;
		int totaldur = dur.get();
		
		
		
		//start time
		cargShip startdur = cargShip.STARTTIME;
		int starttimeHour = startdur.getTime().getHour();
		
		
		//endtime
		cargShip endtimedur= cargShip.ENDTIME;
		LocalTime lt1 = endtimedur.getTime();
//		System.out.println(lt1);
	
		
		//difference b/w 2 duration
		Duration duration = Duration.between(lt, lt1);
		System.out.println(duration.toHours());
		
		totaldur -=duration.toHours();
		ld = ld.plus(1,ChronoUnit.DAYS);
				
		
		
		while(totaldur>=12) {
			for(GovtHoliday gh : GovtHoliday.values())
			{
				
//				if(gh.get().compareTo(ld)==0)
//				{
//					ld = ld.plus(1,ChronoUnit.DAYS);
//					continue;
//				}
				if((gh.get().getMonth()==mo)&&(gh.get().getDayOfMonth()==date)) {
					ld = ld.plus(1,ChronoUnit.DAYS);
					continue;
				}
			}
			
			
			for(Weekend w: Weekend.values()) {
				DayOfWeek dayOfWeek = ld.getDayOfWeek();
				String displayName = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);
				if(w.get().equals(displayName))
				{
					ld = ld.plus(1,ChronoUnit.DAYS);
					continue;
				}
			}
			totaldur -=12;
			ld = ld.plus(1,ChronoUnit.DAYS);
			
		}
		
		String time;
		int checker = (starttimeHour+totaldur)%12;
		if(checker>6 && checker<12) {
			 time = "am";
		}
		else
		{
			time = "pm";
		}
		System.out.println("The ship will reach on "+ld+" at "+checker+" "+time);
		
		return ld;
		
	
	}
}

